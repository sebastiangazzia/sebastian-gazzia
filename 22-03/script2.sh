#!/bin/bash
echo "Test de Conexión a Internet"
echo " "
echo "Verificamos realizando ping a los DNS de Google"
echo " "
RED="ping -c 3 8.8.8.8"
$RED
RED1="ping -c 3 1.1.1.1"
$RED1
echo " "
$MIENV
echo " "
#MIENV1 Variable configurada en .profile 
$MIENV1 
echo " "
SALUDO="Espero que le haya servido"
echo $SALUDO
