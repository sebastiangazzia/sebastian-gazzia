#!/bin/bash

echo "Bienvenido a Nuestra Plataforma Virtual de Libros y Peliculas Empire"
sleep 1s
echo " "
echo "Le solicitamos de nuestra selección de escritores elegir el de su preferencia para así brindarle información del mismo"
sleep 1s
echo " "
echo "(Favor de tener en cuenta que somos una empresa joven y solo contamos con algunos)"
sleep 1s
echo " "
echo "Antes de Comenzar, se debe proceder a instalar GNU GV (Lector PDF)" #Lector PDF algo Rudimentario sin tanto detalle como el ADOBE pero Util
if [ "$(whoami)" != "root" ];then
      echo "Para proceder debe instalar todo como root"
      echo "Podes hacerlo?"
      read -p "[y/n]: " RESP
if [[ "$RESP" == [yY] ]]; then
              REP="sudo apt install gv -y" #Instala APP.
              sudo $REP
              else
              echo "Necesita ser usuario administrador para instalar una aplicación"
              exit 1
fi
fi
echo " "
echo " ------------------------------------------------------------------------------------------------------------------------------------------ "
echo " ------------------------------------------------------------------------------------------------------------------------------------------ "
echo " "
sleep 1s
# --------------------------------------------------------------------------

VAR=( '0)George Lucas' '1)Tolkien' '2)Dan Brown' )

echo ${VAR[@]} #Listalos autores

# --------------------------------------------------------------------------

GL1="George_Lucas"
TK1="Tolkien"
DB1="Dan_Brown"
FECHA=$(date +%d-%m-%y)

#---------------------------------------------------------------------------

read -p "Favor de ingresar el numero del escritor seleccionado:" AUTOR #Me permite elegir el nro del Escritor
if [ "$AUTOR" = "0" ] ; then
      echo "Ha elegido a $GL1"
      test  -d Autores || mkdir Autores
      GL3=Autores/George_Lucas.txt
      echo "Peliculas/Libros de $GL1"             >>$GL3
      echo " "                                    >>$GL3
      echo "- Star Wars la Amenaza Fantasma"      >>$GL3
      echo "- Star Wars la Guerra de los Clones"  >>$GL3
      echo "- Star Wars la Venganza de los Siths" >>$GL3
      echo "- Star Wars una nueva esperanza"      >>$GL3
      echo "- Star Wars el imperio contraataca"   >>$GL3
      echo "- Star Wars el regreso del jedi"      >>$GL3
      echo " "                                    >>$GL3
      echo "Generado por el usuario $USER"        >>$GL3
      echo "Generado en la terminal $(hostname)"  >>$GL3
      echo "Fecha solicitada del doc: $FECHA"     >>$GL3
#---------------------------------------------------------------------------
 elif [ "$AUTOR" = "1" ]; then
      echo "Ha elegido a $TK1"
      test -d Autores || mkdir Autores
      TK3=Autores/Tolkien.txt
      echo "Peliculas/Libros de $TK1"             >>$TK3
      echo " "                                    >>$TK3
      echo "- Trilogia del Señor de los anillos"  >>$TK3
      echo "- Trilogia del Hobbit"                >>$TK3
      echo "- Proximamente el Silmarillion"       >>$TK3
      echo " "                                    >>$TK3
      echo "Generado por el usuario $USER"        >>$TK3
      echo "Generado en la terminal $(hostname)"  >>$TK3
      echo "Fecha solicitada del doc: $FECHA"     >>$TK3
#---------------------------------------------------------------------------
elif [ "$AUTOR" = "2" ]; then
      echo "Ha elegido a $DB1"
      test -d Autores || mkdir Autores
      DB3=Autores/Dan_Brown.txt
      echo "Peliculas/Libros de $DB1"             >>$DB3
      echo " "                                    >>$DB3
      echo "- El Codigo Da Vinci"                 >>$DB3
      echo "- Angeles y Demonios"                 >>$DB3
      echo "- Inferno"                            >>$DB3
      echo " "                                    >>$DB3
      echo "Generado por el usuario $USER"        >>$DB3
      echo "Generado en la terminal $(hostname)"  >>$DB3
      echo "Fecha solicitada del doc: $FECHA"     >>$DB3
# --------------------------------------------------------------------------
else
      echo "No ha seleccionado las opciones indicadas( 0, 1 o 2 )"
      echo " "
      sleep 1s
      echo "Programa Finalizado"
      exit 1
fi
# --------------------------------------------------------------------------
echo " "
echo "Preparando la documentacion del escritor/autor seleccionado"
sleep 1s
echo " "
echo -ne " Procesando la información (33%)\r"
echo -ne " Finalizando el proceso   (100%)\r"
echo " "
echo -ne '\n'
sleep 1s
echo " "
sleep 1s
echo "Muchas Gracias por haber confiado en nosotros"
echo " "
sleep 1s
echo "Que la fuerza te acompañe"
exit
