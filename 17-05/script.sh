#!/bin/bash

echo "Administración General Sistemas"
echo ""
SELECCION="1"
while [ $SELECCION = "1" ] || [ $SELECCION = "2" ] || [ $SELECCION = "3" ] || [ $SELECCION = "4" ] || [ $SELECCION = "5" ] || [ $SELECCION = "6" ]||
[ $SELECCION > "7" ]; do
	clear
       	echo " Seleccione lo que necesita conocer "
	echo " "
	echo " 1) Usuarios conectados y en que terminales "
	echo " "
	echo " 2) Ultima modificación de contraseña de tu usuario "
	echo " "
	echo " 3) Estado del equipo (más detallado que el 1) "
	echo " "
        echo " 4) Listado de usuarios que ingresan al sistema al dia de hoy "
        echo " "
        echo " 5) Mostrar Zona Horaria detallada de donde nos encontramos (Buenos Aires)"
        echo " "
	echo " 6) Salir "
	read -n 1 -p "Favor de Ingresar la opción requerida: "  SELECCION
        echo " "
        echo " "
        read -n 1 -p "Aprete enter y aguarde: " ENTER

INF="texto.txt"
if     [ "$SELECCION" = "1"  ]; then
        echo "Los Usuarios Conectados son : " >> $INF #Quien se encuentra conectado y en que terminales
         who                                  >> $INF #Quien se encuentra conectado y en que terminales
         echo " "                             >> $INF #Quien se encuentra conectado y en que terminales
elif   [ "$SELECCION" = "2"  ]; then
        echo " "                                               >> $INF #Cuando modificó su contraseña el usuario que solicita?
        echo "La Ultima modificación de Contraseña de $USER "  >> $INF #Cuando modificó su contraseña el usuario que solicita?
        chage -l "$USER"                                       >> $INF #Cuando modificó su contraseña el usuario que solicita?
        echo " "                                               >> $INF #Cuando modificó su contraseña el usuario que solicita?
elif   [ "$SELECCION" = "3"  ]; then
        echo " "
        echo"Detalle de usuario conectado, terminales y recursos del sistema " >> $INF #Resumen del estado del equipo
        w                                                                      >> $INF #Resumen del estado del equipo
        echo " "                                                               >> $INF #Resumen del estado del equipo
elif   [ "$SELECCION" = "4"  ]; then
        echo " "
        echo " Listado de usuarios al dia $(date +%d-%m-%y) "
        cat /etc/passwd | grep -f /etc/shells | cut -d: -f 1,6 >> $INF #Listado de usuarios detallado
elif   [ "$SELECCION" = "5"  ]; then
        echo "Zona Horaria en Buenos Aires"                   >> $INF #Zona Horaria en Buenos Aires
        echo "timedatectl list-timezones | grep Buenos_Aires  >> $INF #Información Zona Horaria
        timedatectl
elif   [ "$SELECCION" = "6"  ]; then
        exit 0
elif   [ "$SELECCION" -ge "7" ]; then
        echo " "
        echo "Escriba una opción entre 1 y 6"
        sleep 2s
        continue
fi
done

