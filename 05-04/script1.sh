#!/bin/bash

#Genera usuario
echo "Bienvenido al sistema de gestión de la empresa Springfield"
read -p "Ingrese nombre de usuario a generar:" USUARIO
echo " "
sudo useradd -s /bin/bash -d /home/$USUARIO -m $USUARIO
echo " "

#Genera Contraseña
PEDIDO="Ingrese la contraseña del usuario:"
echo $PEDIDO ; sudo passwd $USUARIO
echo " "

#Genera Directorio del Informe
read -p "Ingrese el dia actual de la semana:" DIA
HOY=$(date +%d-%m-%y_%H-%M)
ACT=$(date +%d-%m-%y)
TMP="tmp/sebastian-gazzia/05-04"
mkdir -vp /$TMP/$DIA-$HOY
cd  /$TMP/$DIA-$HOY
DOC=/$TMP/$DIA-$HOY/informe.txt
touch informe.txt

#Muestra los datos dentro del archivo
echo "Listado Total de usuarios y su directorio" >>$DOC
cat /etc/passwd | cut -d ":" -f 1,6 >>$DOC #Lista todos usuarios con los de sistemas y muestra los generados generados a la fecha
echo " ">>$DOC
echo "Listado de usuarios que ingresan al sistema al dia de hoy"  >>$DOC
cat /etc/passwd | grep -f /etc/shells | cut -d: -f1 >>$DOC #Lista los usuarios que si pueden ingresar. 
echo " ">>$DOC
echo "Ultimo usuario generado" >>$DOC
echo Usuario:$USUARIO >>$DOC ; cat /etc/passwd | grep $USUARIO | cut -d ":" -f 1,6 >>$DOC #Solo muestra el ultimo usuario generado
echo " ">>$DOC
echo "El Usuario fue generado el dia" >>$DOC
echo Creado:$HOY >>$DOC
echo " ">>$DOC

echo " "

sleep 2s

echo "Preparando el Informe"

echo " "

echo -ne "Procesando la información (33%)\r"
sleep 1s
echo -ne "Revisando parametros      (66%)\r"
sleep 2s
echo -ne "Ajustando detalles        (88%)\r"
sleep 2s
echo -ne "Te hace esperar, no?      (90%)\r"
sleep 2s
echo -ne "Ten paciencia             (98%)\r"
sleep 2s
echo -ne "Finalizando el proceso   (100%)\r"
sleep 2s
echo -ne '\n'
echo " "
echo "Ay Caramba !!"
sleep 2s
echo "Informe Generado"
SALUDO="Gracias por haber confiado en nosotros"
SALUDO2="Vuelva Pronto"
echo " "
echo $SALUDO && sleep 2s && echo " " ; echo $SALUDO2
 
